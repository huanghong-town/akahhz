import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'
import preload from '@/common/preload'

Vue.use(VueRouter)

const BkMainEntry = () => import(/* webpackChunkName: 'entry' */'@/views')
const BkNotFound = () => import(/* webpackChunkName: 'none' */'@/views/404')

const horizontalVieweipfgnci = () => import(/* webpackChunkName: 'horizontalView' */'@/views/horizontalView/bkindex')
const bbshome = () => import(/* webpackChunkName: 'bbshome' */'@/views/horizontalView/bbshome.vue')
const bbsdetails = () => import(/* webpackChunkName: 'bbsdetails' */'@/views/horizontalView/bbsdetails.vue')

const routes = [
    {
        path: '/',
        name: 'appMain',
        component: BkMainEntry,
        redirect: { name: 'bbshome' },
        children: [
            {
                path: 'horizontal-nav',
                name: 'horizontalVieweipfgnci',
                component: horizontalVieweipfgnci,
                redirect: { name: 'bbshome' },
                children: [
                    { path: 'bbshome', name: 'bbshome', component: bbshome, meta: { pageName: '论坛首页' } },
                    { path: 'bbsdetails', name: 'bbsdetails', component: bbsdetails, meta: { pageName: '论坛帖子详情' } },
                    { path: '*', component: BkNotFound, meta: { pageName: '404' } }
                ]
            }
        ]
    },
    // 404
    {
        path: '/404',
        name: '404',
        component: BkNotFound,
        meta: {
            pageName: '404'
        }
    },
    {
        path: '*',
        redirect: { name: '404' }
    }
]

const router = new VueRouter({
    mode: 'history',
    base: window.PROJECT_CONFIG.SITE_URL,
    routes: routes
})

let preloading = true
let pageMethodExecuting = true

router.beforeEach(async (to, from, next) => {
    next()
})

router.afterEach(async (to, from) => {
    store.commit('setMainContentLoading', true)

    preloading = true
    await preload()
    preloading = false

    const pageDataMethods = []
    const routerList = to.matched
    routerList.forEach(r => {
        Object.values(r.instances).forEach(vm => {
            if (typeof vm.fetchPageData === 'function') {
                pageDataMethods.push(vm.fetchPageData())
            }
            if (typeof vm.$options.preload === 'function') {
                pageDataMethods.push(vm.$options.preload.call(vm))
            }
        })
    })

    pageMethodExecuting = true
    await Promise.all(pageDataMethods)
    pageMethodExecuting = false

    if (!preloading && !pageMethodExecuting) {
        store.commit('setMainContentLoading', false)
    }

    const meta = to.meta || {}
    document.title = meta.pageName || 'index'
})

export default router
