import { Entity, Column } from 'typeorm'
import Base from './base'

@Entity({ name: 'reply' })
export default class extends Base {
    @Column({ name: 'commentId', type: 'int' })
    'commentId'

    @Column({ name: 'content', type: 'text' })
    'content'
}
