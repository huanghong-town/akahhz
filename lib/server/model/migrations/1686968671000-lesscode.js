const path = require('path')
const { execSql } = require('../../util')

export class Lesscode1686968671000 {
    async up (queryRunner) {
        await execSql(queryRunner, path.resolve(__dirname, './sql/1686968671000-lesscode.sql'))
    }

    async down () {}
}
