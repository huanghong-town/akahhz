const path = require('path')
const { execSql } = require('../../util')

export class Lesscode1683292707000 {
    async up (queryRunner) {
        await execSql(queryRunner, path.resolve(__dirname, './sql/1683292707000-lesscode.sql'))
    }

    async down () {}
}
